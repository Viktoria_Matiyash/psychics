//block scroll for ios
function blockMove() {
    event.preventDefault() ;
}

// adaptive menu
$(".adaptive-menu__open-menu").click(() => {
    $(".adaptive-menu__full-menu").slideDown()
});
$(".adaptive-menu__close-menu").click(() => {
    $(".adaptive-menu__full-menu").slideUp()
});
$(".adaptive-menu__link").click(() => {
    $(".adaptive-menu__full-menu").slideUp()
});

//popup
$(".js-feedback").click((e) => {
    e.preventDefault(), $(".popup__overlay").fadeIn(400, function () {
        $(".popup").css("display", "block").animate({opacity: 1, top: "50%"}, 200)
    })
});
$(".popup__close, .popup__overlay").click(() => {
    $(".popup").animate({opacity: 0, top: "45%"}, 200, function () {
        $(this).css("display", "none"), $(".popup__overlay").fadeOut(400)
    })
});

// for transit
function classFunction(){
    if ($('body').width()>980){
        setTimeout(() => {
            $('.header__logo .js-animate').appendTo( $('.main-block__logo') );
            let para = $('.header__logo .js-animate');
            para.prependTo( '.main-block__logo' );
            $('.header__logo .js-animate').detach().prependTo('.main-block__logo');
            $('.js-animate > g').removeClass('an-1');
            $('.js-animate > g').removeClass('an-2');
            $('.js-animate > g').removeClass('an-3');
            $('.js-animate > g').removeClass('an-4');
            $('.js-animate > g').removeClass('an-5');
        }, 1950);
    }
}
classFunction();
$(window).resize(classFunction);

//for screen switching
var app = (function(app, $){

    var currentScene = 0;

    $cache = {
        scenes: $('.screen')
    }

    function _constructor(){
        _bindEvents();
    }

    $('.main-block__scroll-down').click(function () {
        slideHandle('down');
    });

    $(".adaptive-menu__link").click(function () {
        currentScene = ($(this).data('id'));
        if (   window.location.href !== window.location.origin
            && window.location.href !== window.location.origin + '/#'
            && window.location.href !== window.location.origin + '/') {

            sessionStorage.setItem('current_scene', currentScene);
        }
        sceneHandle();
    });

    $(window).on('load', () => {
        let page = sessionStorage.getItem('current_scene');
        sessionStorage.setItem('current_scene', null);
        if (page !== null && page != 'null') {
            currentScene = parseInt(page);
            sceneHandle();
        }
    });

    function _bindEvents(){
        $('body').swipe({
            swipe: function(event,direction) {
                if(direction == 'up') {
                    slideHandle('down');
                } else if(direction == 'down') {
                    slideHandle('up');
                }
            }
        });
        $('body, a, iframe').on('touchmove',function(e){
            e.stopPropagation();
            e.preventDefault();
        });
        $('a, iframe').on('mouseenter',function(e){
            e.stopPropagation();
            e.preventDefault();
        });
        $.browserSwipe({
            up: function(){
                slideHandle('up');
            },
            down: function(){
                slideHandle('down');
            }
        });
    }

    function slideHandle(direction) {
        switch(direction) {
            case 'up':
                if(currentScene > 0) {
                    currentScene--;
                    sceneHandle();
                }
                break;
            case 'down':
                if(currentScene < ($cache.scenes.length - 1)) {
                    currentScene++;
                    sceneHandle();
                }
                break;
        }
    }

    function sceneHandle(){
        $cache.scenes.each(function(){
            if($(this).index() == currentScene) {
                $(this).addClass('active').removeClass('after');
            } else if($(this).index() < currentScene){
                $(this).addClass('after').removeClass('active');
            } else {
                $(this).removeClass('after').removeClass('active');
            }
        });
    }

    return _constructor;

})(window.app || {}, jQuery)
$(document).ready(function(){
    app();
});

//parallax
$('.layer-one').mousemove(function (e) {
    parallax(e, this, 1);
    parallax(e, document.getElementById('layer-two'), 2);
    parallax(e, document.getElementById('layer-three'), 3);
});

function parallax(e, target, layer) {
    let layer_coeff = 10 / layer;
    let x = ($(window).width() - target.offsetWidth) / 2 - (e.pageX - ($(window).width() / 2)) / layer_coeff;
    let y = ($(window).height() - target.offsetHeight) / 2 - (e.pageY - ($(window).height() / 2)) / layer_coeff;
    $(target).offset({ top: y ,left : x });
}


// tabs
(function($) {
    $(function() {
        $("ul.tabs__caption").on("click", "li:not(.active)", function() {
            $(this)
                .addClass("active")
                .siblings()
                .removeClass("active")
                .closest("div.tabs")
                .find("div.tabs__content")
                .removeClass("active")
                .eq($(this).index())
                .addClass("active");
            let str = "";
            $(".tabs__caption li.active").each(function() {
                str += $(this).text();
            });
            $(".tabs__cap").text( str );
        });
    });
})(jQuery);
let first_li = $('.tabs__caption li:first-child').text();
$('.tabs__cap').text(first_li);

//ajax
function formSend() {
    $('#popup__form .clear').val('');
    $('.popup__notification').show(500);
}

$("form#popup__form").on('submit', function (e) {
    e.preventDefault();
    $.ajax({
        type: 'GET',
        url: '/submit',
        data: $(this).serialize(),
        dataType: "json",
        success: function (data) {
            if (data.status == true) {
                formSend();
            } else {
                alert(data.error_message);
            }
        },
        error: function (error) {
            alert('Ошибка соединения с сервером, попробуйте отправить заявку позже.');
        }
    });
});
$("form#psychics-item-registration__form").on('submit', function (e) {
    e.preventDefault();
    $.ajax({
        type: 'GET',
        url: '/submit',
        data: $(this).serialize(),
        dataType: "json",
        success: function (data) {
            if (data.status == true) {
                $('#psychics-item-registration__form .clear').val('');
                formSend();
            } else {
                alert(data.error_message);
            }
        },
        error: function (error) {
            alert('Ошибка соединения с сервером, попробуйте отправить заявку позже.');
        }
    });
});
$("form#services-item-registration__form").on('submit', function (e) {
    e.preventDefault();
    $.ajax({
        type: 'GET',
        url: '/submit',
        data: $(this).serialize(),
        dataType: "json",
        success: function (data) {
            if (data.status == true) {
                $('#services-item-registration__form .clear').val('');
                formSend();
            } else {
                alert(data.error_message);
            }
        },
        error: function (error) {
            alert('Ошибка соединения с сервером, попробуйте отправить заявку позже.');
        }
    });
});

//sliders
$(".js-slider").slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    autoplay: true,
    infinite: true,
    speed:400,
    nav:true,
    autoplaySpeed:2000,
    margin: 10,
    pauseOnHover: false,
    responsive:[{
        breakpoint:980,
        settings:{
            slidesToShow:2,
            slidesToScroll:1
        }},
        {
            breakpoint:600,
            settings:{
                slidesToShow:2,
                slidesToScroll:1
            }},
        {
            breakpoint:480,
            settings:{
                slidesToShow:1,
                nav:false,
                slidesToScroll:1
            }}]
});

$(".js-slider-services").slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    autoplay: true,
    infinite: true,
    speed:400,
    nav:true,
    dots:true,
    autoplaySpeed:2000,
    margin: 10,
    pauseOnHover: false,
    lazyLoad:'ondemand',
    responsive:[{
        breakpoint:1024,
        settings:{
            slidesToShow:3,
            slidesToScroll:1
        }},
        {
            breakpoint:600,
            settings:{
                slidesToShow:2,
                slidesToScroll:1
            }},
        {
            breakpoint:480,
            settings:{
                slidesToShow:1,
                nav:false,
                slidesToScroll:1}}]
});

$(".js-slider-comments").slick({
    slidesToShow: 2,
    slidesToScroll: 1,
    autoplay: true,
    infinite: true,
    speed:400,
    nav:true,
    dots:false,
    autoplaySpeed:2000,
    pauseOnHover: false,
    lazyLoad:'ondemand',
    responsive:[{
        breakpoint:1024,
        settings:{
            slidesToShow:2,
            slidesToScroll:1
        }},
        {
            breakpoint:800,
            settings:{
                slidesToShow:1,
                slidesToScroll:1
            }},
        {
            breakpoint:420,
            settings:{
                slidesToShow:1,
                slidesToScroll:1}}]
});

//insert text
var text = $('.services__item:nth-child(4) .services__title').text();
$('.slick-dots li:first-child button').text(text);

var text = $('.services__item:nth-child(5) .services__title').text();
$('.slick-dots li:nth-child(2) button').text(text);

var text = $('.services__item:nth-child(6) .services__title').text();
$('.slick-dots li:nth-child(3) button').text(text);

var text = $('.services__item:nth-child(7) .services__title').text();
$('.slick-dots li:nth-child(4) button').text(text);

var text = $('.services__item:nth-child(8) .services__title').text();
$('.slick-dots li:nth-child(5) button').text(text);

var text = $('.services__item:nth-child(9) .services__title').text();
$('.slick-dots li:nth-child(6) button').text(text);

var text = $('.services__item:nth-child(10) .services__title').text();
$('.slick-dots li:nth-child(7) button').text(text);

var text = $('.services__item:nth-child(11) .services__title').text();
$('.slick-dots li:nth-child(8) button').text(text);

// let str2 = "";
// $(".services__title").each(function() {
//     str2 += $(this).text();
// });
//
// let arr = str2.split(' ');
// console.log(arr);
// for (let i = 0; i < arr.length; i++) {
//     console.log(arr[i]);
// }

